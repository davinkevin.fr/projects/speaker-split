import {Component, inject, OnInit} from '@angular/core';
import {presentations, Timing} from '../presentations';
import {from, interval, Observable, sample, Subject, take, timer, withLatestFrom, zip} from 'rxjs';
import {last, map, switchMap, takeUntil} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {DurationPipe} from "./duration/duration.pipe";
import {CommonModule, DecimalPipe} from "@angular/common";

@Component({
  standalone: true,
  imports: [CommonModule, DurationPipe],
  providers: [DecimalPipe],
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {

  private route = inject(ActivatedRoute);

  minutes = 0;
  seconds = 0;

  currentDiff = 0;
  currentSplit: SplitWithTiming | null = null;

  clickOnButton$ = new Subject<void>();

  ngOnInit(): void {
    const [first, ...splits] = presentations
      .find(it => it.id === this.route.snapshot.params['presentationId'])!
      .formats
      .find(it => it.id === this.route.snapshot.params['formatId'])!
      .splits
      .map((value, index, array) => {

        const duration = array
          .filter((_, i) => index > i)
          .map(v => v.duration.minutes * 60 + v.duration.seconds)
          .reduce((prev, curr) => prev + curr, 0)

        const seconds = duration % 60;
        const minutes = (duration - seconds) / 60;

        return {title: value.title, timing: {minutes, seconds}}
      });

    this.currentSplit = first;

    const aTimer: Observable<number> = this.clickOnButton$.pipe(
      take(1),
      switchMap(() => timer(0, 1000)),
    );

    const timingWithSplits: Observable<[SplitWithTiming, number]> = zip(
      from(splits),
      this.clickOnButton$.pipe(withLatestFrom(aTimer, (_, t) => t)),
    );

    aTimer.pipe(takeUntil(timingWithSplits.pipe(last())))
      .subscribe(v => {
        this.seconds = v % 60;
        this.minutes = (v - this.seconds) / 60;
      });

    timingWithSplits.pipe(
      map(([split, timing]: [SplitWithTiming, number]) => {
        const diff = timing - (split.timing.minutes * 60 + split.timing.seconds);
        return {split, diff};
      })
    )
      .subscribe(({diff, split}: IntermediateSplit) => {
        this.currentDiff = diff;
        this.currentSplit = split;
      });
  }

  start() {
    this.clickOnButton$.next();
  }
}

export interface SplitWithTiming {
  timing: Timing;
  title: string;
}

export interface IntermediateSplit {
  split: SplitWithTiming;
  diff: number;
}
