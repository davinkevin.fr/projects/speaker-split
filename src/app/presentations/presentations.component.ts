import {Component, OnInit} from '@angular/core';
import {Presentation, presentations} from '../presentations';
import {CommonModule, NgFor} from "@angular/common";
import {RouterModule, RouterOutlet} from "@angular/router";

@Component({
  standalone: true,
  imports: [CommonModule, RouterModule],
  selector: 'app-presentations',
  templateUrl: './presentations.component.html',
  styleUrls: ['./presentations.component.scss']
})
export class PresentationsComponent implements OnInit {

  presentations: Array<Presentation> = [];

  ngOnInit(): void {
    this.presentations = presentations;
  }

}
