const globalTargetTime = + 40 /* minutes */ * 60 /* + 0 seconds */;

const original= {
    id: "45m",
    duration: "45m",
    splits: [
        {title: "Introduction", duration: {minutes: 0, seconds: 14}},
        {title: "Kubernetes 10 years", duration: {minutes: 0, seconds: 53}},
        {title: "Me Myself and I", duration: {minutes: 1, seconds: 38}},
        {title: "Network", duration: {minutes: 2, seconds: 6}},
        {title: "Kubernetes v1.0", duration: {minutes: 7, seconds: 36}},
        {title: "Alternative Ingresses", duration: {minutes: 3, seconds: 24}},
        {title: "GatewayAPI & Concepts", duration: {minutes: 7, seconds: 26}},
        {title: "CRDs", duration: {minutes: 5, seconds: 50}},
        {title: "Routes", duration: {minutes: 5, seconds: 10}},
        {title: "Deployment Pattern", duration: {minutes: 3, seconds: 5}},
        {title: "Distribution", duration: {minutes: 0, seconds: 58}},
        {title: "Advanced Routing", duration: {minutes: 3, seconds: 43}},
        {title: "How to?", duration: {minutes: 1, seconds: 57}},
        {title: "Ending", duration: {minutes: 0, seconds: 0}}
    ]
}

function convertSecToDuration(duration) {
    const seconds = duration % 60;
    const minutes = (duration - seconds) / 60;
    return {minutes, seconds}
}

function stats(originalDuration, newSplits) {
    const finalDuration = newSplits
        .map(v => v.duration.minutes * 60 + v.duration.seconds)
        .reduce((prev, curr) => prev + curr, 0)

    const original = convertSecToDuration(originalDuration);
    const final = convertSecToDuration(finalDuration);
    console.log(`original duration is: ${original.minutes}m${original.seconds}`)
    console.log(`new duration is: ${final.minutes}m${final.seconds}`)
}

const originalDuration = original
    .splits
    .map(v => v.duration.minutes * 60 + v.duration.seconds)
    .reduce((prev, curr) => prev + curr, 0)

const ratio = globalTargetTime / originalDuration ;

const newSplits = original.splits
    .map(it => {
        const newDuration = Math.floor((it.duration.minutes * 60 + it.duration.seconds) * ratio);
        const seconds = newDuration % 60;
        const minutes = (newDuration - seconds) / 60;
        return {
            ...it,
            duration: {minutes, seconds},
        }
    })


// stats(originalDuration, newSplits)
console.log(JSON.stringify({...original, splits: newSplits}, null, 4))
// console.log({...original, splits: newSplits});

